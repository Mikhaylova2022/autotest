package api;

import com.google.gson.Gson;
import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class Auth extends AbstractTest {

    Gson g = new Gson();

    @Test
    void postAuthUserTest() {
        String s =
                given()
                        .formParam(username, "Mikhaylova")
                        .formParam(password, "08fbea9695")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Auth1 auth1 = g.fromJson(s, Auth1.class);
        System.out.println(auth1.toString());
        assertThat(auth1.username , equalTo("Mikhaylova"));
    }

    @Test
    void postAuthValidTest() {
        String s =
                given()
                        .formParam(username, "1990")
                        .formParam(password, "dc513ea4fb")
                        .request(Method.POST, getUrlLogin())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .asString();
        Auth1 auth1 = g.fromJson(s, Auth1.class);
        System.out.println(auth1.toString());
        assertThat(auth1.username, equalTo("1990"));
    }

    @Test
    void postAuthWithoutLoginAndPasswordTest() {
        given()
                .formParam(username, "")
                .formParam(password, "")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthLengthTest() {
        given()
                .formParam(username, "000000000000000000001")
                .formParam(password, "ce242f7923")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthRuTest() {
        given()
                .formParam(username, "Михайлова")
                .formParam(password, "b4089ef972")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthSpecSymTest() {
        given()
                .formParam(username, "ⓒ")
                .formParam(password, "75848fde84")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthNevalidMinTest() {
        given()
                .formParam(username, "na")
                .formParam(password, "6ec66e124f")
                .request(Method.POST, getUrlLogin())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

    @Test
    void postAuthPasswordTest() {
        given()
                .formParam(username, "Mikhaylova")
                .formParam(password, "нет")
                .request(Method.POST, getUrlLogin()).prettyPeek()
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

}
