package api;

import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class notMyPosts extends AbstractTest {

    @Test
    void getNotMyPostTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .expect()
                        .body(containsString("data"))
                        .body(containsString("id"))
                        .body(containsString("title"))
                        .body(containsString("description"))
                        .body(containsString("content"))
                        .body(containsString("authorId"))
                        .body(containsString("mainImage"))
                        .body(containsString("id"))
                        .body(containsString("cdnUrl"))
                        .body(containsString("updatedAt"))
                        .body(containsString("createdAt"))
                        .body(containsString("labels"))
                        .body(containsString("draft"))
                        .body(containsString("meta"))
                        .body(containsString("prevPage"))
                        .body(containsString("nextPage"))
                        .body(containsString("delayPublishTo"))
                        .body(containsString("count"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
    }

    @Test
    void getPageNullTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(page, "0")
                        .expect()
                        .body(containsString("title"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);

    }

    @Test
    void getNoPageTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "10000000")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.size(), equalTo(0));
        assertThat(posts.meta.nextPage, equalTo(null));
    }

    @Test
    void getSortCreatedAtTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "370")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getOrderASCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(order, ASC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderDESCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(order, DESC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getOrderAllTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationNotMe)
                        .queryParam(order, ALL)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
    }

    @Test
    void getNoAuthTest() {
        given()
                .spec(requestSpecificationNoAuth)
                .queryParam(owner, notMe)
                .expect()
                .body("message", equalTo("Auth header required X-Auth-Token"))
                .when()
                .request(Method.GET, getUrl())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }

}
