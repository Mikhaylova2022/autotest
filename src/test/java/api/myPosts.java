package api;

import io.restassured.http.Method;
import org.junit.jupiter.api.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class myPosts extends AbstractTest {


    @Test
    void getMyPostsTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .expect()
                        .body(containsString("data"))
                        .body(containsString("id"))
                        .body(containsString("title"))
                        .body(containsString("description"))
                        .body(containsString("content"))
                        .body(containsString("authorId"))
                        .body(containsString("mainImage"))
                        .body(containsString("cdnUrl"))
                        .body(containsString("updatedAt"))
                        .body(containsString("createdAt"))
                        .body(containsString("labels"))
                        .body(containsString("draft"))
                        .body(containsString("meta"))
                        .body(containsString("prevPage"))
                        .body(containsString("nextPage"))
                        .body(containsString("delayPublishTo"))
                        .body(containsString("count"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));
        assertThat(posts.data.get(0).authorId, equalTo(getId()));
    }

    @Test
    void getSortCreatedAtTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(sort, createdAt)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getOrderASCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(order, ASC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, lessThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, lessThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, lessThan(0));
    }

    @Test
    void getOrderDESCTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(order, DESC)
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.get(0).id - posts.data.get(1).id, greaterThan(0));
        assertThat(posts.data.get(1).id - posts.data.get(2).id, greaterThan(0));
        assertThat(posts.data.get(2).id - posts.data.get(3).id, greaterThan(0));
    }

    @Test
    void getPage0Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(order, ASC)
                        .queryParam(page, "0")
                        .expect()
                        .body(containsString("title"))
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));

    }

    @Test
    void getPage2Test() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "2")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.meta.prevPage, equalTo(1));

    }

    @Test
    void getNon_existentPageTest() {
        Posts posts =
                given()
                        .spec(requestSpecificationUser)
                        .queryParam(sort, createdAt)
                        .queryParam(page, "50000000000")
                        .expect()
                        .when()
                        .request(Method.GET, getUrl())
                        .then()
                        .spec(responseSpecification)
                        .extract()
                        .body()
                        .as(Posts.class);
        assertThat(posts.data.size(), equalTo(0));
        assertThat(posts.meta.nextPage, equalTo(null));
    }

        @Test
        void getMyPostNullTest() {
            Posts posts =
                    given()
                            .spec(requestSpecification2)
                            .expect()
                            .when()
                            .request(Method.GET, getUrl())
                            .then()
                            .spec(responseSpecification)
                            .extract()
                            .body()
                            .as(Posts.class);
            assertThat(posts.meta.nextPage, equalTo(null));
            assertThat(posts.data.size(), equalTo(0));
    }

    @Test
    void getNoAuthTest() {
        given()
                .spec(requestSpecificationNoAuth)
                .expect()
                .body("message", equalTo("Auth header required X-Auth-Token"))
                .when()
                .request(Method.GET, getUrl())
                .then()
                .spec(responseSpecificationNoAuth)
                .extract()
                .body();
    }
}
