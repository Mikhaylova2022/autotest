package api;

import lombok.Data;

import java.util.List;

@Data
public class Auth1 {
    public Integer id;
    public String username;
    public String token;
    public List<String> roles = null;
}
